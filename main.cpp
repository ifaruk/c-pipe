#include <string>
#include <iostream>

#include "pipe.h"

using Query_A=std::string;
using Resp_A=std::string;

using Query_B=std::string;
using Resp_B=std::string;

using Query_C=std::string;
using Resp_C=std::string;

struct C
{
	
	pipe<Query_C,
	     Resp_C>
	work_pipe()
	{
		return [this](source<Query_C> src,
		              sink<Resp_C> dest)
		{
			src([&](Query_B from_up)
			    {
				    dest("RC_from_C(" + from_up + ")");
			    });
		};
	}
	
	auto
	_do(source<Query_C> a,
	    sink<Resp_C> b)
	{
		return a | work_pipe() | b;
	}
};

struct B
{
	C c;
	
	pipe<Query_B,
	     Query_C>
	process_query()
	{
		return [this](source<Query_B> src,
		              sink<Query_C> dest)
		{
			src([&](Query_B from_up)
			    {
				    dest("C(" + from_up + ")");
			    });
		};
	}
	
	pipe<Query_C,
	     Resp_C>
	do_query()
	{
		return [this](source<Query_C> src,
		              sink<Resp_C> dest)
		{
			auto t = c._do(src,
			               dest);
			t();
		};
	};
	
	pipe<Resp_C,
	     Resp_B>
	format_result()
	{
		return [this](source<Resp_C> src,
		              sink<Resp_B> dest)
		{
			src([&](Resp_C from_down)
			    {
				    dest("RB_from_RC(" + from_down + ")");
			    });
		};
	};
	
	pipe<Query_B,
	     Resp_B>
	work_pipe()
	{
		return process_query() | do_query() | format_result();
	}
	
	auto
	_do(source<Query_B> a,
	    sink<Resp_B> b)
	{
		return a | work_pipe() | b;
	}
};

struct A
{
	B b;
	
	pipe<Query_A,
	     Query_B>
	process_query()
	{
		return [this](source<Query_A> src,
		              sink<Query_B> dest)
		{
			src([&](Query_A from_up)
			    {
				    dest("B(" + from_up + ")");
			    });
		};
	}
	
	pipe<Query_B,
	     Resp_B>
	do_query()
	{
		return [this](source<Query_B> src,
		              sink<Resp_B> dest)
		{
			auto t = b._do(src,
			               dest);
			t();
		};
	};
	
	pipe<Resp_B,
	     Resp_A>
	format_result()
	{
		return [this](source<Resp_B> src,
		              sink<Resp_A> dest)
		{
			src([&](Resp_B from_down)
			    {
				    dest("RA_from_RB(" + from_down + ")");
			    });
		};
	};
	
	pipe<Query_A,
	     Resp_A>
	work_pipe()
	{
		return process_query() | do_query() | format_result();
	}
	
	auto
	_do(Query_A a,
	    sink<Resp_A> b)
	{
		//		simple_source(a) | work_pipe() | b;
		return simple_source(a) | work_pipe() | b;
	}
};

int
main()
{
	Resp_A ar;
	auto   get_reps_ar = [&](Resp_A _a)
	{
		ar = _a;
	};
	
	A a;
	
	auto f = a._do("A(data)",
	               get_reps_ar);
	f();
	
	std::cout << ar << std::endl;
	
	return 0;
}
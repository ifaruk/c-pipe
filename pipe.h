//
// Created by f on 30/10/17.
//

#ifndef CPP17_PIPE_PIPE_H
#define CPP17_PIPE_PIPE_H

#include <utility>
#include <functional>

template<class T,
         class Base=std::function<void(T)>>
struct sink: Base
{
	using Base::operator();
	using Base::Base;
};

template<class T,
         class F>
sink<T>
make_sink(F&& f)
{
	return {std::forward<F>(f)};
}

template<class T> using source=sink<sink<T>>;

template<class T,
         class F>
source<T>
make_source(F&& f)
{
	return {std::forward<F>(f)};
}

template<class T>
source<std::decay_t<T>>
simple_source(T&& t)
{
	return [t = std::forward<T>(t)](auto&& sink)
	{
		return sink(t);
	};
}

template<class In,
         class Out> using pipe = std::function<void(source<In>,
                                                    sink<Out>)>;

template<class In,
         class Out>
sink<In>
operator|(pipe<In,
               Out> p,
          sink<Out> s)
{
	return [p,
	        s](In in)
	{
		p([&](auto&& sink)
		  {sink(std::forward<In>(in));},
		  s);
	};
}

template<class In,
         class Out>
source<Out>
operator|(source<In> s,
          pipe<In,
               Out> p)
{
	return [s,
	        p](auto&& sink)
	{
		p(s,
		  decltype(sink)(sink));
	};
}

template<class T>
std::function<void()>
operator|(source<T> in,
          sink<T> out)
{
	return [in,
	        out]
	{
		in(out);
	};
}

template<class In,
         class Mid,
         class Out>
pipe<In,
     Out>
operator|(pipe<In,
               Mid> a,
          pipe<Mid,
               Out> b)
{
	return [a,
	        b](source<In> src,
	           sink<Out> dest)
	{
		a(src,
		  b | dest);
	};
}

#endif //CPP17_PIPE_PIPE_H
